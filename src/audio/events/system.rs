pub use std::sync::{Mutex, Arc};

use super::{sink::*, AudioEvent};

pub struct AudioEventSystem {
    observers: Mutex<Vec<SinkStorageType>>,
}

impl AudioEventSystem {
    pub fn new() -> AudioEventSystem {
        AudioEventSystem {
            observers: Mutex::new(Vec::new()),
        }
    }
    pub fn attach(&self, observer: SinkStorageType) {
        self.observers.lock().unwrap().push(observer);
    }

    pub fn detach(&self, observer: SinkStorageType) {
        let lock = self.observers.lock();
        let mut observers = lock.unwrap();
        if let Some(idx) = observers.iter().position(|x| eq(x, &observer)) {
            observers.remove(idx);
        }
    }

    pub fn notify(&self, event: AudioEvent) {
        let lock = self.observers.lock();
        let observers = lock.unwrap();
        for item in observers.iter() {
            &{
                let ref this = *item;
                let event = &event;
                this.on_event(event);
            };
        }
    }
}

pub type AudioEventSystemPtr = Arc<AudioEventSystem>;
