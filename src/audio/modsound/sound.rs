use kira::sound::Sound;
use ringbuf::Consumer;

use crate::audio::events::AudioEvent;
use crate::audio::events::AudioEventSystemPtr;

pub use super::Command;
use super::data::*;

pub struct ModSound {
    player_state: mod_player::PlayerState,
    last_line: u32,
    data: ModSoundData,
    playing: bool,
    audioeventsystem: AudioEventSystemPtr,
    command_consumer: Consumer<Command>
}

impl ModSound {
    pub fn new(data: ModSoundData, audioeventsystem: AudioEventSystemPtr, command_consumer: Consumer<Command>)
     -> Self {
        let player_state= mod_player::PlayerState::new(data.song.as_ref().format.num_channels,
                                                       data.sample_rate);
        Self {
            player_state,
            last_line: 9999,
            data,
            playing: false,
            audioeventsystem,
            command_consumer,
        }
    }
}

impl Sound for ModSound {
    fn on_start_processing(&mut self) {
            if self.player_state.current_line != self.last_line {
                let notes  = self.player_state.get_song_line(self.data.song.as_ref());
                self.audioeventsystem.notify(AudioEvent::new_row(notes));
                self.last_line = self.player_state.current_line;
            }
    }

    fn on_clock_tick(&mut self, time: kira::clock::ClockTime) {
    }

    fn track(&mut self) -> kira::track::TrackId {
        kira::track::TrackId::Main
    }

    fn process(&mut self, dt: f64) -> kira::dsp::Frame {
        let (left, right) = mod_player::next_sample(self.data.song.as_ref(), &mut self.player_state);
//        kira::dsp::Frame::new(left, right)
        let tmp = (left + right)/2.;
        kira::dsp::Frame::new(tmp, tmp)
    }

    fn finished(&self) -> bool {
        // we never stop until we are told
//        if self.player_state.song_has_ended {
//            self.audioeventsystem.notify(AudioEvent::MusicEnd);
//        }
        false
    }
}
