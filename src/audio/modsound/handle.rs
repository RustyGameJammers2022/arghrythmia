use ringbuf::Producer;

use super::Command;

pub struct ModSoundHandle {
	pub(super) command_producer: Producer<Command>,
}

impl ModSoundHandle {
    pub fn restart(&mut self) {
        self.command_producer.push(Command::Restart{});
    }

    pub fn play(&mut self) {
        self.command_producer.push(Command::Play);
    }

    pub fn stop(&mut self) {
        self.command_producer.push(Command::Stop);
    }

    pub fn pause(&mut self) {
        self.command_producer.push(Command::Restart);
    }
}
