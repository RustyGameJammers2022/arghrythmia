use std::sync;
use ringbuf::RingBuffer;

use super::super::*;
use modsound::ModSound;

pub struct ModSoundData {
    pub song: sync::Arc<mod_player::Song>,
    pub sample_rate: u32,
    pub channels: u32,
    audioeventsystem: AudioEventSystemPtr,
}

impl ModSoundData {
    pub async fn new(file: &str, audioeventsystem : AudioEventSystemPtr) -> Result<Self, bool> {
        let mod_data = macroquad::file::load_file(file).await;
        match mod_data {
            Ok(mod_file) => {
                let song = sync::Arc::new(mod_player::read_mod_from_data(&mod_file));
                let channels = song.format.num_channels;

                Ok(Self {
                    song,
                    sample_rate: 44100,
                    channels,
                    audioeventsystem,
                })
            },
            Err(_) => { Err(false) },
        }
    }

    pub fn split(self) -> (ModSound, ModSoundHandle) {
        let eventsystem = self.audioeventsystem.clone();
        let (command_producer, command_consumer) = RingBuffer::new(10).split();
        let sound = ModSound::new(self, eventsystem, command_consumer);
        (
            sound,
            ModSoundHandle{command_producer}
        )
    }
}

impl SoundData for ModSoundData {
    type Error = ();

    type Handle = ModSoundHandle;

    fn into_sound(self) -> Result<(Box<dyn Sound>, Self::Handle), Self::Error> {
        let(sound, handle) = self.split();
        Ok((Box::new(sound), handle))
    }
}
