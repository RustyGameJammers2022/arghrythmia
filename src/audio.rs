use kira::{
    manager::{
        AudioManager, AudioManagerSettings,
        backend::cpal::CpalBackend,
    },
    sound::{Sound, SoundData, static_sound::{StaticSoundHandle, StaticSoundSettings, StaticSoundData}},
};
use macroquad::prelude::FileError;

use std::{collections::HashMap, io::Cursor, ops::Deref};
use std::sync::Arc;

pub mod modsound;
pub use modsound::*;
pub mod events;
pub use events::*;

pub type SoundVector = Vec<String>;
pub type SoundHashMap = HashMap<u64, StaticSoundData>;
pub type SoundHandles = Vec<StaticSoundHandle>;

type SoundIdVector = Vec<(u64, f64)>;

#[derive(Clone)]
pub struct SoundQueue {
    actual_queue: SoundIdVector,
}

impl SoundQueue {
    pub fn new() -> SoundQueue {
        SoundQueue { actual_queue : SoundIdVector::new(), }
    }
    pub fn push(&mut self, id : u64) {
        self.actual_queue.push((id, 1.0));
    }

    pub fn push_with_vol(&mut self, id : u64, vol : f64) {
        self.actual_queue.push((id, vol));
    }

    pub fn clear(&mut self) {
        self.actual_queue.clear();
    }
}

pub struct MusicInfo {
    pub channels: u32,
    pub name: String,
}

pub struct AudioConfig {
    pub sounds : SoundVector,
}

pub struct Audio {
    manager: AudioManager::<CpalBackend>,
    modhandle: Option<ModSoundHandle>,
    sounddatas: SoundHashMap,
    soundhandles: SoundHandles,
    audioeventsystem: Arc<AudioEventSystem>,
    music_data: Option<ModSoundData>,
}

type AudioResult<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

impl Audio {
    pub async fn new(config : AudioConfig) -> AudioResult<Audio> {
        let manager = AudioManager::<CpalBackend>::new(AudioManagerSettings::default()).unwrap();

        let mut sounddatas = SoundHashMap::new();
        println!("{} incoming sounds", config.sounds.capacity());
        let mut counter = 0;

        for s in config.sounds.iter() {
            let my_file = macroquad::file::load_file(s).await;
            match my_file {
                Ok(f) => {
                   println!("loaded something");
                    let cursor = Cursor::new(f);
                    let sounddata = StaticSoundData::from_cursor(cursor, StaticSoundSettings::new()).unwrap();
                    sounddatas.insert(counter, sounddata);
                },
                Err(_) => {
                    println!("Not loaded something");
                },
            }
            counter += 1;
        }

        println!("loaded {} sounds", sounddatas.capacity());
        let handles = SoundHandles::new();

        Ok(Audio {
            manager,
            modhandle: None,
            sounddatas,
            soundhandles : handles,
            audioeventsystem: Arc::new(AudioEventSystem::new()),
            music_data: None,
        })
    }

    pub async fn start_music(&mut self, music_file : &String) -> MusicInfo {
        let modsound_data = ModSoundData::new(&music_file, self.audioeventsystem.clone()).await;
        match modsound_data {
            Ok(data) => {
                let retval = MusicInfo {
                    channels : data.song.format.num_channels,
                    name: data.song.name.clone(),
                };
                let handle = self.manager.play(data).unwrap();
                self.modhandle.replace(handle);
                self.audioeventsystem.notify(AudioEvent::MusicStart);
                return retval
            }
            Err(_) => {
                return MusicInfo{channels : 0, name : String::new()}
            }
        }
    }

    pub fn play_soundqueue(&mut self, sounds: SoundQueue) {
        for (k,v) in sounds.actual_queue.iter() {
            match self.sounddatas.get(k) {
                Some(val) => {
                    let mut tmp_handle = self.manager.play(val.clone()).unwrap();
                    let vv = v.clone();
                    tmp_handle.set_volume(kira::Volume::Amplitude(vv),
                                          kira::tween::Tween { ..Default::default() }).unwrap();
                    self.soundhandles.push(tmp_handle);
                    self.audioeventsystem.notify(AudioEvent::SoundEffectPlayed{});
                },
                None => ()
            }
        }
    }

    pub fn attach_observer(&self, observer: SinkStorageType) {
        self.audioeventsystem.attach(observer);
    }

    pub fn detach_observer(&self, observer : SinkStorageType) {
        self.audioeventsystem.detach(observer);
    }
}
