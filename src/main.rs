use std::{f32::consts::PI, sync::Arc, collections::HashMap};
use std::sync::Mutex;

use macroquad::{prelude::*};
use macroquad::rand::{srand, ChooseRandom};

use hecs::{CommandBuffer, World};

use gilrs::{Gilrs, Button, Axis, Event, EventType};

mod observer;

mod components;
use components::*;

mod systems;
use systems::*;

mod audio;
use audio::*;
use audio::events::AudioEvent;

fn window_conf() -> Conf {
    Conf {
        window_title: "BEAT NOID".to_owned(),
        fullscreen: false,
        window_width: 640,
        window_height: 360,
        ..Default::default()
    }
}

pub struct Input {
    up: bool,
    down: bool,
    left: bool,
    right: bool,
    select: bool,
    back: bool,
    cheat: bool,
}
trait AudioObserverInterface {
    fn update(&self);
}

type EventStorage = Vec<AudioEvent>;
struct MyObserver {
    observers: Mutex<EventStorage>,
}

impl MyObserver {
    fn new() -> MyObserver {
        MyObserver {
            observers: Mutex::new(EventStorage::new()),
        }
    }

    fn add(&self, event: &AudioEvent) {
        let lock = self.observers.lock();
        lock.unwrap().push(event.clone());
    }

    fn clear(&self) {
        let lock = self.observers.lock();
        lock.unwrap().clear();
    }

    fn fetch(&self) -> EventStorage {
        let lock = self.observers.lock();
        lock.unwrap().clone()
    }

    fn fetch_and_clear(&self) -> EventStorage {
        let retval = self.fetch();
        self.clear();
        retval
    }
}

impl audio::AudioEventSink for MyObserver {
    fn on_event(&self, event: &AudioEvent) {
        self.add(event);
    }
}

pub struct StateManager {
    states: Vec<Box<dyn State>>
}

pub enum StateTransition {
    NewState(Box<dyn State>),
    ReplaceState(Box<dyn State>),
    PopState
}

impl State for StateManager {
    fn update(&mut self, world: &mut World, context: &mut Context, delta: f32) -> Option<StateTransition> {
        // Delegate update to the top-most state and react to state changes
        if let Some(transition) = self.states.last_mut().unwrap().update(world, context, delta) {
            match transition {
                StateTransition::NewState(mut state) => {
                    state.init(world, context);
                    self.states.push(state);
                },
                StateTransition::ReplaceState(mut state) => {
                    self.states.pop();
                    state.init(world, context);
                    self.states.push(state);
                },
                StateTransition::PopState => {
                    self.states.pop();
                }
            }
        }

        None
    }

    fn draw(&mut self, world: &World, context: &Context) {
        self.states.last_mut().unwrap().draw(world, context);
    }

    fn init(&mut self, world: &mut World, context: &mut Context) {
        self.states.last_mut().unwrap().init(world, context);
    }
}

pub trait State {
    /// Initialize the state, called when the state gets added to the StateManager
    fn init(&mut self, world: &mut World, context: &mut Context);

    /// Update the world and the state based on given input, optionally modifying the state automata
    fn update(&mut self, world: &mut World, context: &mut Context, delta: f32) -> Option<StateTransition>;

    /// Draw the state based on the state of the world
    fn draw(&mut self, world: &World, context: &Context);
}

pub struct GameplayState {
    level_file: String
}

impl State for GameplayState {
    fn update(&mut self, world: &mut World, context: &mut Context, delta: f32) -> Option<StateTransition> {
        paddle_input(world, &context.input);

        handle_wall_collisions(world, delta);

        handle_collisions(world, delta, &mut context.sound_queue);

        destroy_blocks(world, context.misc_textures.get("shard").unwrap(), &mut context.sound_queue);

        spawn_particles(world, delta);

        let mut active_channels = Vec::<u8>::new();
        let mut active_samples = Vec::<u8>::new();
        for event in context.eventbuffer.clone() {
            if let AudioEvent::MusicNewRow { notes } = event {
                let mut i = 0;
                for sample in notes {
                    if sample > 0 {
                        active_channels.push(i);
                        if !active_samples.contains(&sample) {
                            active_samples.push(sample);
                        }
                    }
                    i += 1;
                }
            } else if let AudioEvent::MusicEnd = event {
            }
        }

        // Trigger the paddle lights
        if ([&active_channels, &active_samples][context.triggers.paddle_type as usize]).contains(&context.triggers.paddle) {
            light_up_paddles(world);
        }

        if ([&active_channels, &active_samples][context.triggers.launcher_type as usize]).contains(&context.triggers.launcher) {
            decrement_launch_counter(context);
        }

        // Light up the VU
        light_up_blocks(world, &active_channels, context.block_rows, context.music_info.channels);

        handle_lightables(world, delta);

        check_launch_counter(world, context);

        spawn_new_balls(world, context, delta);

        transition_colors(world, delta);

        move_entities(world, delta);

        spin_entities(world, delta);

        delete_old_particles(world, delta);

        delete_fallen_balls(world);

        if !check_if_balls_in_play(world) {
            Some(StateTransition::ReplaceState(Box::new(GameOverState)))
        } else if !check_if_blocks_in_play(world) {
            Some(StateTransition::ReplaceState(Box::new(VictoryState { level: self.level_file.clone() })))
        } else if context.input.back {
            Some(StateTransition::PopState)
        } else if context.input.cheat {
            if std::env::var("IAMACHEATER").is_ok() {
                Some(StateTransition::ReplaceState(Box::new(VictoryState { level: self.level_file.clone() })))
            } else {
                None
            }
        } else {
            None
        }

    }

    fn draw(&mut self, world: &World, context: &Context) {
        draw_texture(*context.misc_textures.get("bg").unwrap(), 0.0, 0.0, WHITE);
        draw_drawables(world);

        let levelnumtext = format!("LEVEL {}", context.level_number).as_str().to_owned();
        let textsize = measure_text(&levelnumtext, Some(context.font), 14, 1.0);
        draw_text_ex(
            &levelnumtext,
            563.0 - (textsize.width / 2.0),
            160.0,
            TextParams {
                font: context.font,
                font_size: 14,
                font_scale: 1.0,
                font_scale_aspect: 1.0,
                color: color_u8!(255.0, 42.0, 212.0, 255.0),
            }
        );

        let textsize = measure_text(
            &context.level_name,
            Some(context.font),
            11,
            1.0
        );
        draw_text_ex(
            &context.level_name,
            563.0 - (textsize.width / 2.0),
            182.0,
            TextParams {
                font: context.font,
                font_size: 11,
                font_scale: 1.0,
                font_scale_aspect: 1.0,
                color: color_u8!(42.0, 255.0, 212.0, 255.0),
            }
        );

        let pinkparams = TextParams {
            font: context.font,
                font_size: 14,
                font_scale: 1.0,
                font_scale_aspect: 1.0,
                color: color_u8!(255.0, 42.0, 212.0, 255.0)
        };
        let textsize = measure_text("BALL IN", Some(context.font), 14, 1.0);
        draw_text_ex(
            "BALL IN",
            563.0 - (textsize.width / 2.0),
            258.0,
            pinkparams
        );
        let textsize = measure_text(&context.launch_counter.to_string(), Some(context.font), 26, 1.0);
        draw_text_ex(
            &context.launch_counter.to_string(),
            563.0 - (textsize.width / 2.0),
            292.0,
            TextParams {
                font: context.font,
                font_size: 26,
                font_scale: 1.0,
                font_scale_aspect: 1.0,
                color: if context.launch_counter > 3 { color_u8!(42.0, 255.0, 212.0, 255.0) } else { RED }
            }
        );
        let textsize = measure_text("BEATS", Some(context.font), 14, 1.0);
        draw_text_ex(
            "BEATS",
            563.0 - (textsize.width / 2.0),
            315.0,
            pinkparams
        );
    }

    fn init(&mut self, world: &mut World, context: &mut Context) {
        world.clear();
        
        context.signal = Some(EngineSignal::LoadLevel(self.level_file.clone()));
        context.launch_counter = context.launch_count_max;
    }
}

pub struct MainMenuState {
    pub start: bool
}

impl MainMenuState {
    fn new() -> MainMenuState {
        MainMenuState {
            start: false
        }
    }
}

impl State for MainMenuState {
    fn update(&mut self, _world: &mut World, context: &mut Context, _delta: f32) -> Option<StateTransition> {
        if context.input.select {
            let level_file = std::env::var("LEVEL").unwrap_or(String::from("data/levels/t-fishies.toml"));
            Some(StateTransition::NewState(Box::new(GameplayState { level_file })))
        } else {
            None
        }
    }

    fn draw(&mut self, _world: &World, context: &Context) {
        draw_texture(*context.misc_textures.get("startscreen").unwrap(), 0.0, 0.0, WHITE);

        let message = format!("PRESS {} TO PLAY", context.select_prompt);
        let textsize = measure_text(&message, Some(context.font), 16, 1.0);
        draw_text_ex(
            &message,
            320.0 - (textsize.width / 2.0),
            264.0,
            TextParams {
                font: context.font,
                    font_size: 16,
                    font_scale: 1.0,
                    font_scale_aspect: 1.0,
                    color: color_u8!(42.0, 255.0, 212.0, 255.0),
            }
        );
    }


    fn init(&mut self, _world: &mut World, context: &mut Context) {
        context.level_number = 0;
    }
}

pub struct GameOverState;

impl State for GameOverState {
    fn update(&mut self, _world: &mut World, context: &mut Context, _delta: f32) -> Option<StateTransition> {
        if context.input.select {
            Some(StateTransition::NewState(Box::new(MainMenuState::new())))
        } else {
            None
        }
    }

    fn draw(&mut self, _world: &World, context: &Context) {
        draw_texture(*context.misc_textures.get("gameoverscreen").unwrap(), 0.0, 0.0, WHITE);

        let pinkparams = TextParams {
            font: context.font,
                font_size: 11,
                font_scale: 1.0,
                font_scale_aspect: 1.0,
                color: color_u8!(255.0, 42.0, 212.0, 255.0)
        };
        let message = format!("Press {} {}", context.select_prompt, context.gameover_message);
        let textsize = measure_text(&message, Some(context.font), 11, 1.0);
        draw_text_ex(
            &message,
            320.0 - (textsize.width / 2.0),
            330.0,
            pinkparams
        );
    }

    fn init(&mut self, _world: &mut World, _context: &mut Context) {
    }
}

pub struct VictoryState {
    level: String
}

impl State for VictoryState {
    fn update(&mut self, _world: &mut World, context: &mut Context, _delta: f32) -> Option<StateTransition> {
        if context.input.select {
            // TODO: This part determines level flow, probably shouldn't be hard-coded
            let next_level = match self.level.as_str() {
                "data/levels/t-fishies.toml" => "data/levels/m-flowers.toml",
                "data/levels/m-flowers.toml" => "data/levels/t-sadface.toml",
                "data/levels/t-sadface.toml" => "data/levels/t-watermelon.toml",
                "data/levels/t-watermelon.toml" => "data/levels/t-snekky.toml",
                "data/levels/t-snekky.toml" => "data/levels/m-squiggle.toml",
                "data/levels/m-squiggle.toml" => "data/levels/m-seahorsey.toml",
                "data/levels/m-seahorsey.toml" => "data/levels/m-snekface.toml",
                "data/levels/m-snekface.toml" => "data/levels/m-kitty.toml",
                "data/levels/m-kitty.toml" => "data/levels/m-rings.toml",
                "data/levels/m-rings.toml" => "data/levels/t-cross.toml",
                _ => "",
            };

            if next_level == "" {
                return Some(StateTransition::PopState)
            }

            return Some(StateTransition::ReplaceState(Box::new(GameplayState { level_file: String::from(next_level) })));
        } else {
            None
        }
    }

    fn draw(&mut self, _world: &World, context: &Context) {
        draw_texture(*context.misc_textures.get("victoryscreen").unwrap(), 0.0, 0.0, WHITE);

        // TODO: This part determines level flow, probably shouldn't be hard-coded
        let nextlevel = format!("PRESS {} FOR NEXT LEVEL", context.select_prompt);
        let message = if &self.level == &String::from("data/levels/t-cross.toml") { "YOU DID IT! YOU BEAT THE GAME!" } else { &nextlevel };
        let textsize = measure_text(message, Some(context.font), 11, 1.0);
        draw_text_ex(
            message,
            320.0 - (textsize.width / 2.0),
            330.0,
            TextParams {
                font: context.font,
                font_size: 11,
                font_scale: 1.0,
                font_scale_aspect: 1.0,
                color: color_u8!(255.0, 42.0, 212.0, 255.0)
            }
        );
    }


    fn init(&mut self, _world: &mut World, _context: &mut Context) {
    }
}

pub struct GamepadState {
    pub up: bool,
    pub down: bool,
    pub right: bool,
    pub left: bool,
    pub start: bool,
    pub select: bool,
    pub connected: bool,
}

pub struct Triggers {
    pub launcher_type: u8,
    pub launcher: u8,
    pub paddle_type: u8,
    pub paddle: u8,
}

pub struct Context {
    pub input: Input,
    pub sound_queue: SoundQueue,
    pub misc_textures: HashMap<String, Texture2D>,
    pub sound: Audio,
    pub music_info: MusicInfo,
    pub eventbuffer: EventStorage,
    pub block_rows: u8,
    pub signal: Option<EngineSignal>,
    pub font: Font,
    pub launch_count_max: u8,
    pub launch_counter: u8,
    pub triggers: Triggers,
    pub gameover_messages: Vec<String>,
    pub gameover_message: String,
    pub level_name: String,
    pub level_number: u8,
    pub gilrs: Gilrs,
    pub gamepadstate: GamepadState,
    pub select_prompt: String,
}

pub enum EngineSignal {
    LoadLevel(String),
    Quit
}

#[macroquad::main(window_conf)]
async fn main() {
    srand(macroquad::miniquad::date::now() as _);

    let render_target = render_target(640, 360);
    render_target.texture.set_filter(FilterMode::Nearest);

    let mut state_manager = StateManager {
        states: vec![Box::new(MainMenuState::new())]
    };

    set_pc_assets_folder("data");

    let level = initialize_world().await;

    let mut world = level.world;

    let eventsobserver = Arc::new(MyObserver::new());

    let input = Input {
        up: false,
        down: false,
        left: false,
        right: false,
        select: false,
        back: false,
        cheat: false
    };

    let bg = load_texture("images/bg.png").await.unwrap();
    let startscreen = load_texture("images/startscreen.png").await.unwrap();
    let gameoverscreen = load_texture("images/gameover.png").await.unwrap();
    let victoryscreen = load_texture("images/victory.png").await.unwrap();

    let shard = load_texture("images/shard.png").await.unwrap();

    let sound = Audio::new(AudioConfig{sounds : level.leveldescriptor.sounds}).await.unwrap();

    sound.attach_observer(eventsobserver.clone());

    let mut context = Context {
        input,
        sound_queue: SoundQueue::new(),
        misc_textures: HashMap::new(),
        sound,
        music_info: MusicInfo {
            channels: 0,
            name: String::from("")
        },
        eventbuffer : Vec::new(),
        block_rows: level.block_rows,
        signal: None,
        font: load_ttf_font("BoomBoxFree.ttf").await.unwrap(),
        launch_count_max: level.launcher_countdown,
        launch_counter: level.launcher_countdown,
        triggers: Triggers {
            launcher_type: level.launcher_trigger_type,
            launcher: level.launcher_trigger,
            paddle_type: level.paddle_trigger_type,
            paddle: level.paddle_trigger,
        },
        gameover_messages: vec![
            String::from("TO WALLOW IN REGRET"),
            String::from("TO SLINK AWAY IN SHAME"),
            String::from("TO LEARN FROM YOUR MISTAKES"),
            String::from("TO MOPE LIKE A BOSS"),
            String::from("TO PRETEND THIS NEVER HAPPENED"),
            String::from("TO RALLY YOUR SPIRITS"),
            String::from("TO RAGEQUIT"),
            String::from("FOR PITY POINTS"),
            String::from("TO FEEL BETTER"),
            String::from("TO PERSEVERE"),
            String::from("TO FIND COURAGE"),
            String::from("TO REFLECT ON YOUR FAILURE"),
        ],
        gameover_message: String::new(),
        level_name: String::new(),
        level_number: 0,
        gilrs: Gilrs::new().unwrap(),
        gamepadstate: GamepadState {
            up: false,
            down: false,
            right: false,
            left: false,
            start: false,
            select: false,
            connected: false,
        },
        select_prompt: String::from("ENTER"),
    };
    context.gamepadstate.connected = context.gilrs.gamepads().count() > 0;

    let ball = load_texture("images/ball.png").await.unwrap();

    let incoming1 = load_texture("images/incoming-1.png").await.unwrap();
    let incoming2 = load_texture("images/incoming-2.png").await.unwrap();
    let incoming3 = load_texture("images/incoming-3.png").await.unwrap();

    context.misc_textures.insert(String::from("shard"), shard);
    context.misc_textures.insert(String::from("bg"), bg);
    context.misc_textures.insert(String::from("startscreen"), startscreen);
    context.misc_textures.insert(String::from("gameoverscreen"), gameoverscreen);
    context.misc_textures.insert(String::from("victoryscreen"), victoryscreen);

    context.misc_textures.insert(String::from("ball"), ball);
    context.misc_textures.insert(String::from("incoming-1"), incoming1);
    context.misc_textures.insert(String::from("incoming-2"), incoming2);
    context.misc_textures.insert(String::from("incoming-3"), incoming3);

    context.music_info = context.sound.start_music(&level.leveldescriptor.music).await;

    let mut cam = Camera2D::from_display_rect(Rect {x: 0.0, y: 0.0, w: 640.0, h: 360.0});
    cam.render_target = Some(render_target);

    'game: loop {
        set_camera(&cam);

        if let Some(signal) = &mut context.signal {
            match signal {
                EngineSignal::LoadLevel(level_path) => {
                    let level = components::load_level(&level_path).await.unwrap();
                    context.block_rows = level.block_rows;
                    context.triggers.launcher_type = level.launcher_trigger_type;
                    context.triggers.launcher = level.launcher_trigger;
                    context.triggers.paddle_type = level.paddle_trigger_type;
                    context.triggers.paddle = level.paddle_trigger;
                    context.launch_count_max = level.launcher_countdown;
                    context.launch_counter = level.launcher_countdown;
                    context.gameover_message = context.gameover_messages.choose().unwrap().to_string();
                    context.level_name = level.leveldescriptor.name;
                    context.level_number += 1;
                    world = level.world;

                },
                EngineSignal::Quit => break 'game,
            }
        }
        context.signal = None;

        let delta = get_frame_time();

        context.eventbuffer = eventsobserver.as_ref().fetch_and_clear();

        // GAME UPDATES
        update_input(&mut context.input, &mut context.gilrs, &mut context.gamepadstate, &mut context.select_prompt);

        state_manager.update(&mut world, &mut context, delta);

        context.sound.play_soundqueue(context.sound_queue.clone());

        context.sound_queue.clear();

        // RENDERING
        state_manager.draw(&world, &context);

        set_default_camera();

        clear_background(BLACK);

        let width = screen_width().min(screen_height() * (16.0 / 9.0));
        let height = screen_height().min(screen_width() * (9.0 / 16.0));
        draw_texture_ex(
            render_target.texture,
            0.0,
            0.0,
            WHITE,
            DrawTextureParams {
                // Flip vertically to work around https://github.com/not-fl3/macroquad/issues/171
                flip_y: true,
                dest_size: Some(vec2(width.max(640.0), height.max(360.0))),
                ..Default::default()
            },
        );

        next_frame().await
    }
}

// TODO: I hate this fucking function, it needs to die in a fire
async fn initialize_world() -> components::Level {
    components::load_level("data/levels/t-fishies.toml").await.unwrap()
}
