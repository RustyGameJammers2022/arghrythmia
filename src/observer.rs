
pub trait ObserverInterface {
    fn update(&self);
}

pub trait SubjectInterface<'a, T: ObserverInterface> {
    fn attach(&mut self, observer: &'a T);
    fn detach(&mut self, observer: &'a T);
    fn notify_observers(&self);
}

pub struct Subject<'a, T: ObserverInterface> {
    observers: Vec<&'a T>,
}

impl<'a, T: ObserverInterface + PartialEq> Subject<'a, T> {
    fn new() -> Subject<'a, T> {
        Subject {
            observers: Vec::new(),
        }
    }
}

impl<'a, T: ObserverInterface + PartialEq> SubjectInterface<'a, T> for Subject<'a, T> {
    fn attach(&mut self, observer: &'a T) {
        self.observers.push(observer);
    }

    fn detach(&mut self, observer: &'a T) {
        if let Some(idx) = self.observers.iter().position(|x| *x == observer) {
            self.observers.remove(idx);
        }
    }

    fn notify_observers(&self) {
        for item in self.observers.iter() {
            item.update();
        }
    }
}
